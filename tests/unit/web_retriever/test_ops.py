from unittest.mock import MagicMock

import pytest


async def test_get(mock_hub, hub):
    mock_hub.web_retriever.ops.get = hub.web_retriever.ops.get

    url = "mock_url"
    headers = {}

    mock_hub.web_retriever.rss.get_feed.return_value = {}
    mock_hub.web_retriever.web.request.return_value = {}

    data, ret = await mock_hub.web_retriever.ops.get(url, headers, method="get")
    assert data == {"content": {}, "type": "text", "url": "mock_url"}
    assert ret is False
    mock_hub.web_retriever.web.request.assert_called_once()

    mock_hub.web_retriever.web.request.reset_mock()

    data, ret = await mock_hub.web_retriever.ops.get(url, headers, method="get", type="json")
    assert data == {"content": {}, "type": "json", "url": "mock_url"}
    assert ret is False
    assert mock_hub.web_retriever.web.request.call_args.kwargs["content_type"] == "json"
    mock_hub.web_retriever.web.request.assert_called_once()

    mock_hub.web_retriever.web.request.reset_mock()

    data, ret = await mock_hub.web_retriever.ops.get(url, headers, method="get", type="rss")
    assert data == {"content": {}, "type": "rss", "url": "mock_url"}
    assert ret is False
    mock_hub.web_retriever.rss.get_feed.assert_called_once()
    mock_hub.web_retriever.web.request.assert_not_called()


@pytest.mark.parametrize(
    "headers,mods,expected",
    [
        [{}, [], {}],
        [{"foo": "bar"}, [], {}],
        [{"foo": "bar"}, [{"pass_request_headers": True}], {"foo": "bar"}],
        [
            {"foo": "bar"},
            [{"request_headers": {"add": {"fizz": "buzz"}}}],
            {"fizz": "buzz"},
        ],
        [{"foo": "bar"}, [{"request_headers": {"remove": ["foo"]}}], {}],
        [{"foo": "bar"}, [{"request_headers": {"remove": ["baz"]}}], {}],
    ],
)
async def test_mods(hub, headers, mods, expected):
    ret = hub.web_retriever.ops.mods(headers, mods)
    assert ret == expected


@pytest.mark.parametrize(
    "attr,val,rules,expected",
    [
        ["remote", "127.0.0.1", [], []],
        [
            "remote",
            "127.0.0.1",
            [
                {
                    "rule_type": "deny",
                    "rule_string": "remote == '127.0.0.1'",
                },
            ],
            {"error": "forbidden by pre rule 0"},
        ],
        [
            "remote",
            "10.0.0.1",
            [
                {
                    "rule_type": "deny",
                    "rule_string": "remote == '127.0.0.1'",
                },
            ],
            [],
        ],
        [
            "remote",
            "127.0.0.1",
            [
                {
                    "rule_type": "allow",
                    "rule_string": "remote == '127.0.0.1'",
                },
            ],
            [],
        ],
        [
            "remote",
            "10.0.0.1",
            [
                {
                    "rule_type": "allow",
                    "rule_string": "remote == '127.0.0.1'",
                },
            ],
            {"error": "forbidden by pre rule 0"},
        ],
        [
            "remote",
            "127.0.0.1",
            [
                {
                    "rule_type": "allow",
                    "rule_string": "<><",
                },
            ],
            {"error": "pre rule 0 failure"},
        ],
        [
            "remote",
            "127.0.0.1",
            [
                {
                    "rule_type": "transform",
                    "transform": "stuffhere",
                },
            ],
            ["stuffhere"],
        ],
    ],
)
async def test_process_rules(hub, attr, val, rules, expected):
    request = MagicMock()
    setattr(request, attr, val)

    ret = hub.web_retriever.ops.process_rules(request, rules)

    assert isinstance(ret, (dict, list))
    assert ret == expected
