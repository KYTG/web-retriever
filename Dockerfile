# Build web-retriever binary
FROM python:3.10-buster as tiamat
ARG PIP_CONFIG
WORKDIR /build
RUN apt-get update && apt-get install -y build-essential
RUN pip3 install --no-cache-dir tiamat

RUN mkdir /root/.pip/; echo "$PIP_CONFIG" > /root/.pip/pip.conf;
COPY . .
RUN tiamat build -n web-retriever -r requirements/base.txt --pyinstaller-version 5.0.1

# Assemble final image
# TODO: use scratch image. need to copy in linked system libraries
#FROM scratch
FROM bitnami/minideb:buster-amd64
RUN apt-get update && apt-get install -y ca-certificates
COPY --from=tiamat /build/dist/web-retriever /
CMD ["/web-retriever"]
